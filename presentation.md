% Gallerie GitLab-CI
% Vincent Rouvreau
% Ingénieur SED Saclay

---

## GitLab

De nombreux "outils" autour de cette forge:

 * CI/CD
 * Registry: containers, packages, ...
 * pages
 * shared runners
 * ...

---

## CI

Historiquement sur le portail https://ci.inria.fr et avec Jenkins

![](img/portail_CI_gitlab.png)


---

## GitLab-CI

Un fichier de configuration `.gitlab-ci.yml`:

 * à la racine d'un dépôt git
 * peut inclure d'autres fichiers `.yml`
 * *macros*
 * très bonne documentation

---

## [Gallerie GitLab-CI](https://gitlab.inria.fr/gitlabci_gallery/intro/-/blob/main/README.md), mais pourquoi ?

 * formation
 * copier/coller

---

## [LaTeX collaboratif](https://gitlab.inria.fr/gitlabci_gallery/latex/latex-beamer)

```mermaid
%%{init: { 'theme': 'dark', 'gitGraph': {'showBranches': true, 'showCommitLabel':false }} }%%
gitGraph
    commit
    commit
    branch intro
    branch conclusion
    checkout intro
    commit
    checkout conclusion
    commit
    checkout intro
    commit
    checkout conclusion
    commit
    checkout main
    merge intro
    checkout conclusion
    commit
    checkout main
    merge conclusion
```

---

## Docker in docker (1/3)

![](img/docker_in_docker.png)

---

## [Docker in docker](https://gitlab.inria.fr/inria-ci/docker/-/raw/main/register-dockerfile/register-dockerfile.yml) (2/3)

```mermaid
%%{init: { 'theme': 'dark', 'gitGraph': {'showBranches': true, 'showCommitLabel':false }} }%%
gitGraph
    commit
    commit
    branch develop
    checkout develop
    commit tag: "project:develop"
    commit
    checkout main
    merge develop tag: "rm project:develop" type: REVERSE
    commit
    commit
```

---

## Docker in docker (3/3)

 * docker+machine n'est plus maintenu
 * disponibilité

---

## [Custom runner](https://gitlab.inria.fr/inria-ci/custom-runner) 1/2

 * en bêta
 * linux / osx / windows(wsl)
 * docker

---

## [Custom runner](https://gitlab.inria.fr/vrouvrea/custom-runner) 2/2

 * en cours

---

## [API cloudstack](https://gitlab.inria.fr/gitlabci_gallery/orchestration/cloudstack-api)

 * Créer / Démarrer / Arrêter des VMs sur Cloudstack
 * Pour aller plus loin : [ci-cmdline](https://gitlab.inria.fr/inria-ci/ci-cmdline)

---

## Et vous ?

 * [thématiques](https://gitlab.inria.fr/gitlabci_gallery) manquantes ?
 * Prêts à contribuer ?
